#include "TESTLANG_DIR/common/dbprint.hpp"
#include "TESTLANG_DIR/parser/language.hpp"
#include "TESTLANG_DIR/parser/parser.hpp"
#include "TESTLANG_DIR/syntax/syntax.hpp"

#include <iostream>

namespace NS_TESTLANG { namespace TEST {

void go(const std::string& file_content) {
  Parser::Parser p(file_content);

  Common::DBPrintVisitor printer;

  p.program->accept(printer);

  printer.print();
}
}} // namespace NS_TESTLANG::TEST

int main(int argc, char* argv[]) { NS_TESTLANG::TEST::go(argv[1]); }