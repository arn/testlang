#pragma once

/* =========================
 * Visitor macro definitions
 * =========================
 *
 * This header is only intended for utility functions and macros relating to the
 * visitor pattern. It is forbidden from ever including other headers, except
 * for portable utilities the standard library.
 */

#define VISITEE_FOR(x)               \
  x& accept(x& v) {                  \
    v.visit(*this, nullptr);         \
    return v;                        \
  }                                  \
  x& accept(x& v, void* interface) { \
    v.visit(*this, interface);       \
    return v;                        \
  }

#define VIRT_VISITEE_FOR(x)    \
  virtual x& accept(x& v) = 0; \
  virtual x& accept(x& v, void* interface) = 0;

#define VISITOR_FOR(x)                   \
  void visit(x& other, void* interface); \
  void visit(x& other) { visit(other, nullptr); }

#define VISITOR_IMPL(x, y) void x::visit(y& other, void* interface)
