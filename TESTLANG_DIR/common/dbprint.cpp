#include "dbprint.hpp"

#include "TESTLANG_DIR/parser/language.hpp"
#include "TESTLANG_DIR/syntax/syntax.hpp"

#ifndef OUT
#define OUT ss << indentations()
#else
#error DBPrint: OUT was already defined
#endif

namespace NS_TESTLANG {
using Common::DBPrintVisitor;

/***** Parser::Language *****/
VISITOR_IMPL(DBPrintVisitor, Parser::Language::Number) {
  OUT << "NUMBER(" << other.str_representation << ")" << std::endl;
}
VISITOR_IMPL(DBPrintVisitor, Parser::Language::Keyword) {
  OUT << "KEYWORD(";
  try {
    OUT << Parser::Language::Keyword::TYPE_STR_MAP.at(other.type);
  } catch (...) {
    OUT << "ERR";
  }
  OUT << ")" << std::endl;
}
VISITOR_IMPL(DBPrintVisitor, Parser::Language::Symbol) {
  OUT << "SYMBOL(" << other.display_name << ")" << std::endl;
}

/***** Syntax *****/
VISITOR_IMPL(DBPrintVisitor, Syntax::Program) {
  OUT << "Program:" << std::endl;
  inc_indent();
  for (auto& f : other.functions) {
    f->accept(*this);
  }
  dec_indent();
}
VISITOR_IMPL(DBPrintVisitor, Syntax::Function) {
  OUT << "Function:" << std::endl;
  inc_indent();
  for (auto& e : other.expressions) {
    e->accept(*this);
  }
  dec_indent();
}
VISITOR_IMPL(DBPrintVisitor, Syntax::Symbol) {
  OUT << "Symbol: " << other.name << std::endl;
}
VISITOR_IMPL(DBPrintVisitor, Syntax::Definition) {
  OUT << "Definition:" << std::endl;
  inc_indent();
  OUT << "LHS:";
  other.lhs->accept(*this);
  OUT << "RHS:";
  other.rhs->accept(*this);
  dec_indent();
}
VISITOR_IMPL(DBPrintVisitor, Syntax::Atomic) { OUT << "Atomic:" << std::endl; }

/***** Internal print methods *****/

void DBPrintVisitor::inc_indent() { indentation_level++; }

void DBPrintVisitor::dec_indent() {
  if (indentation_level == 0) {
    throw std::runtime_error("Attempted to reduce indentation below 0");
  }
  indentation_level--;
}

std::string DBPrintVisitor::indentations() const {
  return std::string(indentation_level, ' ');
}

void DBPrintVisitor::print() { std::cout << ss.str(); }

} // namespace NS_TESTLANG

#undef OUT