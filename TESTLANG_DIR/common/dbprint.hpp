#pragma once

#include "forward.hpp"
#include "visitor.hpp"

#include <iostream>
#include <sstream>

namespace NS_TESTLANG { namespace Common {
struct DBPrintVisitor {
  VISITOR_FOR(Parser::Language::Token)
  VISITOR_FOR(Parser::Language::Number)
  VISITOR_FOR(Parser::Language::Keyword)
  VISITOR_FOR(Parser::Language::Symbol)
  VISITOR_FOR(Syntax::Program)
  VISITOR_FOR(Syntax::Function)
  VISITOR_FOR(Syntax::Symbol)
  VISITOR_FOR(Syntax::Definition)
  VISITOR_FOR(Syntax::Atomic)

  void print();

private:
  unsigned indentation_level = 0;
  std::stringstream ss;
  std::string indentations() const;
  void inc_indent();
  void dec_indent();
};
}} // namespace NS_TESTLANG::Common