#pragma once

#include "visitor.hpp"

/// Global forward declarations

namespace NS_TESTLANG {
namespace Common {
struct DBPrintVisitor;
}
namespace Compiler {
struct Context;
struct CompilerVisitor;
struct Compiler;
} // namespace Compiler
namespace Parser { namespace Language {
struct Token;
struct Number;
struct Keyword;
struct Symbol;
}} // namespace Parser::Language
namespace Syntax {
struct AST;
struct Program;
struct Function;
struct Symbol;
struct Definition;
struct Atomic;
} // namespace Syntax
} // namespace NS_TESTLANG