#pragma once

#include "TESTLANG_DIR/common/dbprint.hpp"
#include "TESTLANG_DIR/common/forward.hpp"
#include "TESTLANG_DIR/compiler/compiler_visitor.hpp"

#include <string>
#include <vector>

#define AST_VISITEE                   \
  VISITEE_FOR(Common::DBPrintVisitor) \
  VISITEE_FOR(Compiler::CompilerVisitor)

namespace NS_TESTLANG { namespace Syntax {

struct AST {
  virtual ~AST() = default;
  VIRT_VISITEE_FOR(Common::DBPrintVisitor)
  VIRT_VISITEE_FOR(Compiler::CompilerVisitor)
};

struct Program final : public AST {
  AST_VISITEE
  std::vector<std::unique_ptr<Function>> functions;
};

struct Symbol final : public AST {
  AST_VISITEE
  std::string name;
};

struct Expression : public AST {};

struct Function final : public AST {
  AST_VISITEE
  std::vector<std::unique_ptr<Expression>> expressions;
};

struct Definition : public Expression {
  AST_VISITEE
  std::shared_ptr<Symbol> lhs;
  std::unique_ptr<Expression> rhs;
};

struct Atomic final : public Expression {
  AST_VISITEE
  enum Primitive { u64 };
};
}} // namespace NS_TESTLANG::Syntax