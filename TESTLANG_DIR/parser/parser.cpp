#include <tao/pegtl.hpp>
#include <tao/pegtl/analyze.hpp>

#include "TESTLANG_DIR/syntax/syntax.hpp"

#include "language.hpp"
#include "parser.hpp"

#include <sstream>

namespace pegtl = tao::TAO_PEGTL_NAMESPACE;
using namespace pegtl;

namespace NS_TESTLANG { namespace Parser {

namespace Grammar {

// Forward definitions
struct expression;
// End forward definitions

// Begin utility
template <typename T> struct seq_at : seq<at<T>, T> {};
// End utility

// Begin atomics
struct fn : seq_at<pegtl::string<'f', 'n'>> {};
struct let : seq_at<pegtl::string<'l', 'e', 't'>> {};
struct print : seq_at<pegtl::string<'p', 'r', 'i', 'n', 't'>> {};
struct lparen : seq_at<pegtl::string<'('>> {};
struct rparen : seq_at<pegtl::string<')'>> {};
struct lbrace : seq_at<pegtl::string<'{'>> {}; // '
struct rbrace : seq_at<pegtl::string<'}'>> {};
struct eq : seq_at<pegtl::string<'='>> {};
// End atomics

// Begin grammar internals
struct reserved : sor<fn, let, lparen, rparen, lbrace, rbrace, eq> {};

struct ws : star<space> {};
struct weol : star<sor<ascii::space, ascii::eol>> {};

struct number : seq<opt<sor<one<'-'>, one<'+'>>>, plus<digit>> {};
struct name : seq<not_at<reserved>, plus<alpha>> {};
// End grammar internals

/***** Begin language *****/
/** Begin expressions **/
struct ex_paren : seq_at<seq<lparen, ws, expression, ws, rparen>> {};
struct ex_print : seq_at<seq<print, ws, lparen, ws, expression, ws, rparen>> {};
struct ex_definition : seq_at<seq<let, ws, name, ws, eq, ws, number>> {};
/** End expressions **/

struct expression : sor<ex_paren, ex_print, ex_definition> {};
struct linear_expression : seq<expression, weol> {};

struct function
    : seq_at<seq<fn, weol, name, weol, eq, weol, lparen, weol, rparen, weol,
                 lbrace, weol, plus<linear_expression>, weol, rbrace>> {};

struct grammar : must<function> {};
/***** End language *****/

// Default single rule
template <typename Rule> struct action : pegtl::nothing<Rule> {};

/***** Begin token actions *****/
static void push_token(const std::string& in_str, Parser& parser) {
  parser.tokens.push_back(std::make_unique<Language::Keyword>(in_str));
}
// TODO: fold these into smaller section if possible
template <> struct action<fn> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    push_token(in.string(), parser);
  }
};
template <> struct action<let> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    push_token(in.string(), parser);
  }
};
template <> struct action<lparen> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    push_token(in.string(), parser);
  }
};
template <> struct action<rparen> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    push_token(in.string(), parser);
  }
};
template <> struct action<lbrace> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    push_token(in.string(), parser);
  }
};
template <> struct action<rbrace> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    push_token(in.string(), parser);
  }
};
template <> struct action<eq> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    push_token(in.string(), parser);
  }
};
template <> struct action<name> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    parser.tokens.push_back(std::make_unique<Language::Symbol>(in.string()));
  }
};
template <> struct action<number> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
    parser.tokens.push_back(std::make_unique<Language::Number>(in.string()));
  }
};
/***** End token actions *****/

template <typename T> static std::unique_ptr<T> back_token(Parser& parser) {
  auto* x = parser.tokens.back().get();
  if (T* cast_type = dynamic_cast<T*>(x)) {
    parser.tokens.pop_back();
    return std::make_unique<T>(*cast_type);
  }
  throw std::runtime_error("Failed to enforce type");
}

static void discard_keyword(Parser& parser, Language::Keyword::Type type) {
  if (auto* x = dynamic_cast<Language::Keyword*>(parser.tokens.back().get())) {
    if (x->type == type) {
      parser.tokens.pop_back();
      return;
    }
    throw std::runtime_error(
        "Encountered incorrect keyword type: " + x->str() +
        ". Expected: " + Language::Keyword::string_from_type(type));
  }
  throw std::runtime_error("Failed to enforce type");
}
template <> struct action<ex_paren> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
#ifndef DB_NO_CONSUME_TOKENS
    discard_keyword(parser, Language::Keyword::RParen);
    discard_keyword(parser, Language::Keyword::LParen);
#endif
  }
};
template <> struct action<ex_print> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
#ifndef DB_NO_CONSUME_TOKENS
    discard_keyword(parser, Language::Keyword::RParen);
    discard_keyword(parser, Language::Keyword::LParen);
    discard_keyword(parser, Language::Keyword::Print);
#endif
  }
};
template <> struct action<ex_definition> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
#ifndef DB_NO_CONSUME_TOKENS
    auto rhs = back_token<Language::Number>(parser);
    discard_keyword(parser, Language::Keyword::Equals);
    auto lhs = back_token<Language::Symbol>(parser);
    discard_keyword(parser, Language::Keyword::Let);
// TODO: push expression to function
#endif
  }
};

template <> struct action<function> {
  template <typename Input>
  static void apply(const Input& in, Parser& parser, Syntax::Program& program) {
#ifndef DB_NO_CONSUME_TOKENS
    discard_keyword(parser, Language::Keyword::RBrace);
    discard_keyword(parser, Language::Keyword::LBrace);
    discard_keyword(parser, Language::Keyword::RParen);
    discard_keyword(parser, Language::Keyword::LParen);
    discard_keyword(parser, Language::Keyword::Equals);
    auto name = back_token<Language::Symbol>(parser);
    discard_keyword(parser, Language::Keyword::Fn);
    program.functions.push_back(std::make_unique<Syntax::Function>());
#endif
  }
};

} // namespace Grammar

Parser::Parser(const std::string& file_contents) {
  populate_tokens_from_file(file_contents);
}

void Parser::populate_tokens_from_file(const std::string& s) {
  pegtl::analyze<Grammar::grammar>();
  pegtl::file_input<> fileInput(s);
  this->program = std::make_unique<Syntax::Program>();

  // The parser assumes that there is at least one function in the program
  this->program->functions.push_back(std::make_unique<Syntax::Function>());

  parse<Grammar::grammar, Grammar::action>(fileInput, *this, *this->program);

  // An empty function was pushed to the back of the functions list when the
  // final function was parsed
  this->program->functions.pop_back();
}

}} // namespace NS_TESTLANG::Parser