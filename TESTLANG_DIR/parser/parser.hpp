#pragma once

#include "TESTLANG_DIR/common/forward.hpp"

#include <memory>
#include <string>
#include <vector>

namespace NS_TESTLANG { namespace Parser {

struct Parser {
  Parser(const std::string& s);

  const std::vector<std::unique_ptr<Language::Token>>& get_tokens() const {
    return tokens;
  }

  std::vector<std::unique_ptr<Language::Token>> tokens;

  std::unique_ptr<Syntax::Program> program;

private:
  void populate_tokens_from_file(const std::string& s);
};

}} // namespace NS_TESTLANG::Parser
