#include "language.hpp"

namespace NS_TESTLANG { namespace Parser { namespace Language {

std::unordered_map<std::string, Keyword::Type> Keyword::STR_TYPE_MAP = {
    {"let", Let},
    {"fn", Fn},
    {"print", Print},
    {",", Comma},
    {"\n", Newline},
    {"=", Equals},
    {"{", LBrace},
    {"}", RBrace},
    {"(", LParen},
    {")", RParen},
};

std::unordered_map<Keyword::Type, std::string> Keyword::TYPE_STR_MAP = {
    {InvalidType, "InvalidType"},
    {Let, "let"},
    {Fn, "fn"},
    {Print, "print"},
    {Comma, "comma"},
    {Newline, "newline"},
    {Equals, "equals"},
    {LBrace, "l_brace"},
    {RBrace, "r_brace"},
    {LParen, "l_paren"},
    {RParen, "r_paren"},
};

std::string Keyword::string_from_type(Keyword::Type t) {
  try {
    return TYPE_STR_MAP.at(t);
  } catch (std::out_of_range& _) {
    return "ERR_" + std::to_string(t);
  }
}

Keyword::Type Keyword::type_from_string(const std::string& s) {
  if (is_type(s)) {
    return STR_TYPE_MAP.at(s);
  }
  return InvalidType;
}

std::string Keyword::str() const { return string_from_type(type); }
}}} // namespace NS_TESTLANG::Parser::Language