#pragma once

#include "TESTLANG_DIR/common/dbprint.hpp"

#include <memory>
#include <string>
#include <unordered_map>
#include <utility>

namespace NS_TESTLANG { namespace Parser { namespace Language {

struct Token {
  virtual ~Token() = default;
  VIRT_VISITEE_FOR(Common::DBPrintVisitor)
};

struct Number : Token {
  VISITEE_FOR(Common::DBPrintVisitor)

  std::string str_representation;

  Number(std::string s) : str_representation(std::move(s)) {}
};

struct Keyword : public Token {
  VISITEE_FOR(Common::DBPrintVisitor)

  enum Type {
    InvalidType = 0,
    Comma,
    Newline,
    Equals,
    LBrace,
    RBrace,
    LParen,
    RParen,
    Let,
    Fn,
    Print,
  };
  Type type;

  static std::unordered_map<std::string, Keyword::Type> STR_TYPE_MAP;
  static std::unordered_map<Keyword::Type, std::string> TYPE_STR_MAP;
  static std::string string_from_type(Type);

  std::string str() const;

  Keyword(Type type) : type(type) {}
  Keyword(const std::string& s) : type(type_from_string(s)) {}

private:
  static Type type_from_string(const std::string& s);
  static bool is_type(const std::string& s) {
    return Keyword::STR_TYPE_MAP.count(s) != 0U;
  }
};

struct Symbol : public Token {
  VISITEE_FOR(Common::DBPrintVisitor)

  std::string display_name;

  Symbol(std::string s) : display_name(std::move(s)) {}
};
}}} // namespace NS_TESTLANG::Parser::Language