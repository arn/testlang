#pragma once

#include "compiler_visitor.hpp"

#include "TESTLANG_DIR/common/forward.hpp"

#include "llvm/IR/IRBuilder.h" // cannot forward declare IRBuilder

#include <memory>

namespace llvm {
struct LLVMContext;
struct Module;
} // namespace llvm

namespace NS_TESTLANG { namespace Compiler {

struct Context {};

struct Compiler {
  friend class CompilerVisitor;
  Compiler(const Syntax::Program&);

private:
  void compile();

  CompilerVisitor visitor_;

  llvm::LLVMContext llvm_context_;
  llvm::IRBuilder<> builder_ = llvm::IRBuilder<>(llvm_context_);
  std::unique_ptr<llvm::Module> llvm_module_;
};

}} // namespace NS_TESTLANG::Compiler