#pragma once

#include "TESTLANG_DIR/common/forward.hpp"
#include "TESTLANG_DIR/common/visitor.hpp"

namespace NS_TESTLANG { namespace Compiler {

struct CompilerVisitor {
  VISITOR_FOR(Syntax::Program)
  VISITOR_FOR(Syntax::Function)
  VISITOR_FOR(Syntax::Symbol)
  VISITOR_FOR(Syntax::Definition)
  VISITOR_FOR(Syntax::Atomic)

  CompilerVisitor(Compiler&);

  Compiler& compiler;
};

}} // namespace NS_TESTLANG::Compiler