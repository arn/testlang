add_library(compiler compiler.cpp compiler_visitor.hpp)

find_package(LLVM REQUIRED CONFIG)
message(STATUS "Found LLVM ${LLVM_PACKAGE_VERSION}")
message(STATUS "Using LLVMConfig.cmake in: ${LLVM_DIR}")

target_include_directories(compiler PUBLIC ../../)

target_link_libraries(compiler LLVM common)
