#include "compiler.hpp"
#include "compiler_visitor.hpp"

#include "TESTLANG_DIR/syntax/syntax.hpp"

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"

namespace NS_TESTLANG { namespace Compiler {

VISITOR_IMPL(CompilerVisitor, Syntax::Program) {
  for (auto& func : other.functions) {
    func->accept(*this);
  }
}
VISITOR_IMPL(CompilerVisitor, Syntax::Function) {
  auto* function_type = llvm::FunctionType::get(
      llvm::Type::getInt64Ty(compiler.llvm_context_), false);
  auto* function = llvm::Function::Create(function_type,
                                          llvm::Function::ExternalLinkage,
                                          "main",
                                          compiler.llvm_module_.get());
  auto* entry = llvm::BasicBlock::Create(
      compiler.llvm_context_, "function_entry", function);

  for (auto& expr : other.expressions) {
    expr->accept(*this);
  }

  llvm::verifyFunction(*function);
}
VISITOR_IMPL(CompilerVisitor, Syntax::Symbol) {}
VISITOR_IMPL(CompilerVisitor, Syntax::Definition) {}
VISITOR_IMPL(CompilerVisitor, Syntax::Atomic) {}

CompilerVisitor::CompilerVisitor(Compiler& compiler) : compiler(compiler) {}

Compiler::Compiler(const Syntax::Program& program) : visitor_(*this) {}

}} // namespace NS_TESTLANG::Compiler