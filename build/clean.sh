ROOT=$(dirname $(realpath $0))
echo "Cleaning $ROOT"
rm -rf `find $ROOT -not -path $ROOT -not -name README -not -name .gitignore -not -name clean.sh`
