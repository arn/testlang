PROJECT_ROOT=$(realpath $(dirname $(realpath $0))/..)

echo "Running format.sh"
$PROJECT_ROOT/tools/format.sh
echo "Running lint.sh"
$PROJECT_ROOT/tools/lint.sh
