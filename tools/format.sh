#!/bin/bash

PROJECT_ROOT=$(realpath $(dirname $(realpath $0))/..)

echo "Formatting C++"
find $PROJECT_ROOT/TESTLANG_DIR/ -iname *.?pp | xargs clang-format -i

echo "Formatting CMakeLists"
find $PROJECT_ROOT -name CMakeLists.txt | xargs cmake-format -i
