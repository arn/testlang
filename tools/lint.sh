#!/bin/bash

PROJECT_ROOT=$(realpath $(dirname $(realpath $0))/..)

cd $PROJECT_ROOT || (echo "Failed to cd" && exit)
MODIFIED_CPP=$(git status --porcelain | awk 'match($1, /M|A|MM|AM/){print $2}' | grep .cpp\$)

CHECKS="modernize-*,bugprone-*,clang-analyzer-*,performance-*,portability-*,readability-*,-modernize-use-trailing-return-type"

if [[ $* == --all ]]; then
  echo "Passing all cpp files to clang-tidy"
  MODIFIED_CPP=$(find $PROJECT_ROOT/TESTLANG_DIR/ -iname *.cpp)
else
  echo "Passing files modified since last commit to clang-tidy"
fi

FIX=""
if [[ $* == --fix ]]; then
  echo "Passing --fix to clang-tidy"
  FIX="--fix"
fi

echo "Starting clang-tidy"
clang-tidy \
  -warnings-as-errors=* \
  -header-filter=$PROJECT_ROOT/TESTLANG_DIR/.* \
  -checks=${CHECKS} \
  $MODIFIED_CPP \
  $FIX \
  -- \
  -I "${execution_root}/external/pegtl" \
  -I .
